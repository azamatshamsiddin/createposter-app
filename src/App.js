import './styles/App.css';
import PostList from "./components/PostList";
import React, { useEffect, useState } from "react";
import PostForm from "./components/PostForm";
import PostFilter from "./components/PostFilter";
import MyModal from "./components/UI/modal/MyModal";
import MyButton from "./components/UI/button/MyButton";
import { usePosts } from "./hooks/usePosts";
import { PostService } from "./API/PostService";
import Loader from "./components/UI/Loader/Loader";
import { useFetch } from "./hooks/useFetch";
import { getPageCount } from "./utils/pages";
import Pagination from "./components/UI/pagination/Pagination";


function App() {
  const [posts, setPosts] = useState([])
  const [filter, setFilter] = useState({sort: '', query: ''})
  const [modal, setModal] = useState(false)
  const sortedAndSearchedPosts = usePosts(posts, filter.sort, filter.query)
  const [totalPages, setTotalPages] = useState(0)
  const [limit, setLimit] = useState(10)
  const [page, setPage] = useState(1)
  
  const [fetchPosts, isPostsLoading, postError] = useFetch(async (limit, page) => {
    const response = await PostService.getAll(limit, page);
    setPosts(response.data)
    const totalCount = response.headers['x-total-count']
    setTotalPages(getPageCount(totalCount, limit))
  })
  
  useEffect(() => {
    fetchPosts(limit, page)
  }, [])
  
  
  const createPost = (newPost) => {
    setPosts([...posts, newPost])
    setModal(false)
  }
  
  const deletePost = (post) => {
    setPosts(posts.filter(p => p.id !== post.id))
  }
  
  const changePage = (page) => {
    setPage(page)
    fetchPosts(limit, page)
  }
  
  return (
    <div className={"App"}>
      <MyButton style={{marginTop: "30px"}} onClick={() => setModal(true)}>
        Create New Post
      </MyButton>
      <MyModal visible={modal} setVisible={setModal}>
        <PostForm create={createPost}/>
      </MyModal>
      <hr style={{margin: "20px 0"}}/>
      <PostFilter filter={filter} setFilter={setFilter}/>
      {
        postError && <h1 align={"center"}>Can't found posts {postError}</h1>
      }
      {
        isPostsLoading
          ? <div style={{display: "flex", justifyContent: 'center', marginTop: 50}}><Loader/></div>
          : <PostList deletePost={deletePost} posts={sortedAndSearchedPosts} title={"List of posts"}/>
      }
      <Pagination page={page} totalPages={totalPages} changePage={changePage}/>
    </div>
  );
}

export default App;
